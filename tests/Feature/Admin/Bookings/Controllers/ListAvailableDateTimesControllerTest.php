<?php

namespace Admin\Bookings\Controllers;

use App\Admin\Bookings\Controllers\ListAvailableDateTimesController;
use Domain\Booking\Models\AvailableDateTime;
use Tests\TestCase;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListAvailableDateTimesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create([
            'is_admin' => true,
        ]);
        $this->be($user);
    }

    public function testListAvailableDateTimesControllerTest()
    {
        $availableDateTime = AvailableDateTime::factory()->create();


        $res = $this->json('GET', "admin/bookings/available-dates/{$availableDateTime->available_date_id}/times");

        $res->assertOk();
        $this->assertSame(1, count($res->json()));
    }

}
