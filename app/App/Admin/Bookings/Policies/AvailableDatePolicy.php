<?php

namespace App\Admin\Bookings\Policies;

use Domain\User\Models\User;

class AvailableDatePolicy
{
    public function viewAny(User $authedUser)
    {
        return $authedUser->is_admin;
    }

    public function addOrUpdate(User $authedUser)
    {
        return $authedUser->is_admin;
    }
}
