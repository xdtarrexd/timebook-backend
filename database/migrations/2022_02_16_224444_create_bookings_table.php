<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->id();
            $table->string('state'); // PENDING, ACCEPTED, DENIED
            $table->foreignId('customer_id')->constrained();
            $table->foreignId('requested_date_id')->constrained('available_dates');
            $table->foreignId('requested_date_time_id')->constrained('available_date_times');
            $table->foreignId('requested_user_id')->nullable()->constrained('users');
            $table->timestamps();
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
