<?php

namespace Domain\Product\Actions;

use Domain\Product\DataTransferObjects\ProductDTO;
use Domain\Product\Models\Product;

class StoreProductAction
{
	public function __invoke(ProductDTO $DTO): Product
	{
		$product = new Product;
        $product->fromDTO($DTO);
        $product->save();
        return $product;
	}
}
