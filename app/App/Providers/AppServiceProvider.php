<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
               * Today we learned that Carbon and PHP is utter garbage
               */
        Carbon:: useMonthsOverflow(false);
        Schema::defaultStringLength(191);
        Carbon::setLocale('sv');
        $appEnv = config('app.env');
        if ($appEnv == 'production' || $appEnv == 'stage') {
            $this->app['request']->server->set('HTTPS', 'on');
        }

        /*
         * Please laravel
         */
        Factory::guessFactoryNamesUsing(function (string $modelName) {
            $cls = class_basename($modelName);
            return "Database\\Factories\\{$cls}Factory";
        });

    }
}
