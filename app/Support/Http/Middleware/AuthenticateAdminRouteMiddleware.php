<?php

namespace Support\Http\Middleware;

use Auth;
use Domain\User\Models\User;
use Domain\User\States\User\Active;
use Domain\User\States\User\Inactive;
use Illuminate\Http\Request;


class AuthenticateAdminRouteMiddleware
{
    public function handle(Request $request, $next)
    {
        // attempt to login
        $this->login($request);
        // pass the request
        return $next($request);
    }

    /**
     * @param Request $request
     * @return void
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    protected function login(Request $request): void
    {
        if (Auth::hasUser()) {
            return;
        }
        $accessToken = $this->getAccessToken($request);
        if (is_null($accessToken)) {
            abort(400, 'No Access-Token provided');
        }

        $user = User::whereAccessToken($accessToken)->first();

        if (is_null($user)) {
            abort(410, 'Invalid access-token');
        }

        if ($user->state instanceof (Inactive::class)) {
            abort(410, 'Account not active');
        }

        Auth::setUser($user);
    }

    protected function getAccessToken(Request $request): string|null
    {
        return $request->headers->get('Access-Token', $request->get('Access-Token'));
    }
}