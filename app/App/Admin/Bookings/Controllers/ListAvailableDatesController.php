<?php

namespace App\Admin\Bookings\Controllers;

use App\Admin\Bookings\Policies\AvailableDatePolicy;
use App\Admin\Bookings\Queries\ListAvailableDatesQuery;
use Domain\Booking\Models\AvailableDate;
use Support\Http\AuthorizeSingleControllerMethod;

class ListAvailableDatesController extends AuthorizeSingleControllerMethod
{
    protected $model = AvailableDate::class;
    protected $policy = AvailableDatePolicy::class;

	public function __invoke(ListAvailableDatesQuery $query)
	{
        $this->authorize('viewAny', $this->model);
        return $query->search()->paginate();
	}
}
