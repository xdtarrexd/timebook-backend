<?php

namespace App\Admin\Bookings\Queries;

use Domain\Booking\Models\AvailableDate;
use Domain\Booking\Models\AvailableDateTime;
use Laravel\Scout\Builder as ScoutBuilder;
use Support\BaseQueryBuilder;

class ListAvailableDateTimesQuery extends BaseQueryBuilder
{

    private AvailableDate $availableDate;

    public function withAvailableDate(AvailableDate $availableDate): self
    {
        $this->availableDate = $availableDate;
        return $this;
    }

    public function base()
    {
        return AvailableDateTime::whereAvailableDateId($this->availableDate->id);
    }

}