<?php

namespace Domain\Product\Enums;

enum ProductTypeEnums
{
    case SERVICE;
    case PRODUCT;

    public function toString()
    {
        return match ($this) {
            ProductTypeEnums::SERVICE => 'Tjänst',
            ProductTypeEnums::PRODUCT => 'Produkt',
        };
    }
}