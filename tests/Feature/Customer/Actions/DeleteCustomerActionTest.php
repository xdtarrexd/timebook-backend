<?php

namespace Customer\Actions;

use Domain\Customer\Actions\DeleteCustomerAction;
use Domain\Customer\Models\Customer;
use Tests\TestCase;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DeleteCustomerActionTest extends TestCase
{
    use RefreshDatabase;

    public function testDeleteCustomerActionTest()
    {
        $customer = Customer::factory()->create();

        (new DeleteCustomerAction)($customer);

        $this->assertDeleted($customer);

    }

}
