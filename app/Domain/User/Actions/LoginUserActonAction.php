<?php

namespace Domain\User\Actions;

use Domain\User\DataTransferObjects\LoginDTO;
use Domain\User\Exceptions\InvalidUsernameOrPasswordException;
use Domain\User\Models\User;
use Hash;

class LoginUserActonAction
{
    /**
     * @param LoginDTO $DTO
     * @return User
     * @throws InvalidUsernameOrPasswordException
     */
    public function __invoke(LoginDTO $DTO): User
    {
        $user = User::whereEmail($DTO->username)->first();

        if (!$user) {
            throw new InvalidUsernameOrPasswordException;
        }

        if (!Hash::check($DTO->password, $user->password)) {
            throw new InvalidUsernameOrPasswordException;
        }

        return $user;
    }
}
