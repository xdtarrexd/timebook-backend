<?php

namespace Domain\User\Actions;

use Domain\User\DataTransferObjects\UserDTO;
use Domain\User\Models\User;
use Support\Models\BaseModelTrait;

class StoreUserAction
{

    public function __invoke(UserDTO $DTO): User
    {
        $user = (new User)->fromDTO($DTO);

        return $user;
    }

}