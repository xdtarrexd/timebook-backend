<?php

namespace Domain\Booking\Observers;

use Domain\Booking\Exceptions\InvalidDateTimeException;
use Domain\Booking\Models\AvailableDateTime;
use Support\ValidatorHelpers\CheckIfMilitaryTime;

class AvailableDateTimeObserver
{
    public function saving(AvailableDateTime $availableDateTime)
    {
        if (!CheckIfMilitaryTime::validate($availableDateTime->time)) {
            throw new InvalidDateTimeException;
        }
    }

}