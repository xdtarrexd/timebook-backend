<?php

namespace Admin\Users\Controllers;

use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateUserPasswordControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create([
            'is_admin' => true,
        ]);
        $this->be($user);
    }

    public function testUpdateUserPasswordControllerWithWrongPassword()
    {
        $user = User::factory()->create([
            'password' => 'secret'
        ]);

        $res = $this->json('PATCH', "admin/users/{$user->id}/update-password", [
            'old_password' => 'not secret',
            'new_password' => 123,
            'new_password_again' => 123,
        ]);

        $res->assertStatus(422);
        // validate that password is the same
        $user->refresh();
        $this->assertTrue(\Hash::check('secret', $user->password));

    }
    public function testUpdateUserPasswordControllerWithCorrectPassword()
    {
        $user = User::factory()->create([
            'password' => 'secret'
        ]);

        $res = $this->json('PATCH', "admin/users/{$user->id}/update-password", [
            'old_password' => 'secret',
            'new_password' => 123,
            'new_password_again' => 123,
        ]);

        $res->assertOk();
        // validate that password is changed
        $user->refresh();
        $this->assertTrue(\Hash::check('123', $user->password));

    }

}
