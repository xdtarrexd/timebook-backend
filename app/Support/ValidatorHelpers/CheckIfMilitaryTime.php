<?php

namespace Support\ValidatorHelpers;

class CheckIfMilitaryTime
{

    /**
     * Will only validate fully qualified HH:MM
     * @param $input
     * @return bool
     */
    public static function validate($input): bool
    {
        return preg_match('/^(2[0-3]|[01]?[0-9]):([0-5]?[0-9]{2,})$/', $input) === 1;
    }

}