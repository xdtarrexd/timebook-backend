<?php

namespace Tests\Unit\User\Actions;

use Domain\User\Actions\UpdateUserPasswordAction;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateUserPasswordActionTest extends TestCase
{
    use RefreshDatabase;

    public function testUpdateUserPassword()
    {
        $user = User::factory()->create([
            'password' => 123
        ]);

        $this->assertTrue(\Hash::check(123, $user->password));

        (new UpdateUserPasswordAction)($user, 123, 456);
        $user->refresh();

        $this->assertTrue(\Hash::check(456, $user->password));
    }

}
