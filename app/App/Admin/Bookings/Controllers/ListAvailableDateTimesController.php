<?php

namespace App\Admin\Bookings\Controllers;

use App\Admin\Bookings\Policies\AvailableDatePolicy;
use App\Admin\Bookings\Queries\ListAvailableDateTimesQuery;
use Domain\Booking\Models\AvailableDate;
use Support\Http\AuthorizeSingleControllerMethod;

class ListAvailableDateTimesController extends AuthorizeSingleControllerMethod
{
    protected $model = AvailableDate::class;
    protected $policy = AvailableDatePolicy::class;

    public function __invoke(ListAvailableDateTimesQuery $query, AvailableDate $availableDate)
    {
        $this->authorize('viewAny', $this->model);
        return $query->withAvailableDate($availableDate)->get();
    }
}