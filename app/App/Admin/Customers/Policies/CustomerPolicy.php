<?php

namespace App\Admin\Customers\Policies;

use Domain\Customer\Models\Customer;
use Domain\User\Models\User;

class CustomerPolicy
{
	public function viewAny(User $authedUser): bool
	{
		return true;
	}


	public function view(User $authedUser, Customer $customer): bool
	{
		return true;
	}


	public function create(User $authedUser): bool
	{
		return true;
	}


	public function update(User $authedUser, Customer $customer): bool
	{
		return $authedUser->is_admin;
	}


	public function delete(User $authedUser, Customer $customer): bool
	{
		return $authedUser->is_admin;
	}
}
