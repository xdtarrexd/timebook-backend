<?php

namespace Domain\User\Models;

use Domain\User\Observers\UserObserver;
use Domain\User\States\User\UserState;
use Eloquent;
use Hash;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Laravel\Scout\Searchable;
use Spatie\ModelStates\HasStates;
use Support\Models\BaseModelTrait;

/**
 * Domain\User\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $access_token
 * @property mixed $state
 * @property bool $is_bookable
 * @property bool $is_admin
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read mixed $state_color
 * @method static \Database\Factories\UserFactory factory(...$parameters)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereAccessToken($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereIsAdmin($value)
 * @method static Builder|User whereIsBookable($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User whereNotState(string $column, $states)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereState($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 */
class User extends Model implements Authenticatable
{
    use HasFactory, HasStates, Searchable, BaseModelTrait;

    protected $guarded = [];

    protected $hidden = ['access_token', 'password'];

    protected $appends = ['state_color'];

    protected $casts = [
        'is_admin' => 'bool',
        'is_bookable' => 'bool',
        'state' => UserState::class,
    ];

    public static function boot()
    {
        parent::boot();
        self::observe(UserObserver::class);
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = Hash::make($value);
    }

    public function getStateColorAttribute()
    {
        return $this->state->color();
    }

    public function getAuthIdentifierName()
    {
        return $this->name;
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        // TODO: Implement getAuthPassword() method.
    }

    public function getRememberToken()
    {
        // TODO: Implement getRememberToken() method.
    }

    public function setRememberToken($value)
    {
        // TODO: Implement setRememberToken() method.
    }

    public function getRememberTokenName()
    {
        // TODO: Implement getRememberTokenName() method.
    }
}