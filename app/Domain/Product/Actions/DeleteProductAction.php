<?php

namespace Domain\Product\Actions;

use Domain\Product\Models\Product;

class DeleteProductAction
{
	public function __invoke(Product $product): void
	{
        $product->delete();
	}
}
