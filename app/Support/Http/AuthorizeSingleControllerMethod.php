<?php

namespace Support\Http;

use Gate;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class AuthorizeSingleControllerMethod
{
    use AuthorizesRequests;

    protected $model;
    protected $policy;

    public function __construct()
    {
        if (is_null($this->model) || is_null($this->policy)) {
            throw new \Exception('Set model and policy to use this controller');
        }
        Gate::policy($this->model, $this->policy);
    }
}