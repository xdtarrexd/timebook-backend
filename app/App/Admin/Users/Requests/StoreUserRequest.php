<?php

namespace App\Admin\Users\Requests;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreUserRequest extends FormRequest
{
    public function rules()
    {
        $userId = data_get($this->route('user'), 'id');

        return [
            'email' => ['required', 'email', Rule::unique('users')->ignore($userId)],
            'name' => ['required', Rule::unique('users')->ignore($userId)],
            'is_bookable' => ['required', 'boolean'],
            'is_admin' => ['required', 'boolean'],
        ];
    }

}