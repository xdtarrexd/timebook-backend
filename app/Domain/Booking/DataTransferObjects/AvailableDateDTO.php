<?php

namespace Domain\Booking\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\Attributes\CastWith;
use Spatie\DataTransferObject\DataTransferObject;

final class AvailableDateDTO extends DataTransferObject
{
    public string $date;
    public bool $published;
    /* @var AvailableDateTimeDTO[] */
    #[CastWith(CollectionOfAvailableDateTimesDTO::class)]
    public array $times;


    /**
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            date: $request->get('date'),
            published: $request->get('published'),
            times: $request->get('times', []),
        );
    }


}
