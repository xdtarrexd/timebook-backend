<?php

namespace Domain\User\Exceptions;

use Support\Exceptions\DomainException;

class InvalidUsernameOrPasswordException extends DomainException
{
}
