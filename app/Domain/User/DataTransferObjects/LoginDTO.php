<?php

namespace Domain\User\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

final class LoginDTO extends DataTransferObject
{
    public string $username;
    public string $password;

    public static function fromRequest(Request $request): self
    {
        return new self(
            username: $request->get('username'),
            password: $request->get('password')
        );
    }

}
