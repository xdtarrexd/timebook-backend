<?php

namespace Database\Factories;

use Domain\Booking\Models\AvailableDate;
use Domain\Booking\Models\AvailableDateTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class AvailableDateTimeFactory extends Factory
{
    protected $model = AvailableDateTime::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'available_date_id' => AvailableDate::factory(),
            'time' => '13:37',
            'duration' => $this->faker->numberBetween(60, 90)
        ];
    }
}
