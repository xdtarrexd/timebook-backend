<?php

namespace Domain\Product\Actions;

use Domain\Product\DataTransferObjects\ProductDTO;
use Domain\Product\Models\Product;

class UpdateProductAction
{
	public function __invoke(Product $product, ProductDTO $DTO): void
	{
		$product->fromDTO($DTO);
        $product->save();
	}
}
