<?php

namespace Domain\User\DataTransferObjects;

use Illuminate\Foundation\Http\FormRequest;
use Spatie\DataTransferObject\DataTransferObject;

class UserDTO extends DataTransferObject
{
    public string $name;
    public string $email;
    public bool $is_bookable;
    public bool $is_admin;

    public static function fromRequest(FormRequest $request): self
    {
        return new static([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'is_bookable' => $request->get('is_bookable'),
            'is_admin' => $request->get('is_admin'),
        ]);
    }
}