<?php

namespace Product\Actions;

use Domain\Product\Actions\StoreProductAction;
use Domain\Product\DataTransferObjects\ProductDTO;
use Domain\Product\Enums\ProductTypeEnums;
use Domain\Product\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StoreProductActionTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreProductActionTest()
    {

        $dto = new ProductDTO(
            name: 'Klipp herr 60 min',
            price: '320',
            vat: '25',
            quantity: 60,
            unit: 'st',
            type: ProductTypeEnums::SERVICE->toString(),
            description: '60 min med rakapparat',
        );
        $res = (new  StoreProductAction)($dto);

        $this->assertInstanceOf(Product::class, $res);

    }

}
