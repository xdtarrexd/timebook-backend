<?php

namespace App\Admin\Customers\Controllers;

use App\Admin\Customers\Policies\CustomerPolicy;
use App\Admin\Customers\Queries\CustomerIndexQuery;
use App\Admin\Customers\Requests\UpdateCustomerRequest;
use Domain\Customer\Actions\DeleteCustomerAction;
use Domain\Customer\Actions\UpdateCustomerAction;
use Domain\Customer\DataTransferObjects\CustomerDTO;
use Domain\Customer\Models\Customer;
use Support\Http\AuthorizeCurdController;

class CustomerController extends AuthorizeCurdController
{
    protected $model = Customer::class;
    protected $policy = CustomerPolicy::class;

    public function index(CustomerIndexQuery $query)
    {
        return $query->search()->paginate();
    }

    public function show(Customer $customer)
    {
        return $customer;
    }

    public function update(UpdateCustomerRequest $request, Customer $customer, UpdateCustomerAction $action)
    {
        $dto = CustomerDTO::fromRequest($request);
        $action($customer, $dto);
    }

    public function destroy(Customer $customer, DeleteCustomerAction $action)
    {
        $action($customer);
    }
}
