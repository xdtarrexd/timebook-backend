<?php

namespace Support\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\DataTransferObject\DataTransferObject;

/**
 * Support\Models\BaseModel
 *
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModelTrait newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModelTrait newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BaseModelTrait query()
 * @mixin \Eloquent
 */
trait BaseModelTrait
{

    public function fromDTO(DataTransferObject $dto): self
    {
        return $this->fill($dto->toArray());
    }

}