<?php

namespace Database\Factories;

use Domain\Booking\Models\AvailableDate;
use Illuminate\Database\Eloquent\Factories\Factory;

class AvailableDateFactory extends Factory
{
    protected $model = AvailableDate::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'date' => $this->faker->date('Y-m-d'),
            'published' => $this->faker->boolean,
        ];
    }
}
