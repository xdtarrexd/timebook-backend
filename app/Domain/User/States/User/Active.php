<?php

namespace Domain\User\States\User;

class Active extends UserState
{

    public function color(): string
    {
        return 'green';
    }
}