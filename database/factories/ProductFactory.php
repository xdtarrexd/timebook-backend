<?php

namespace Database\Factories;

use Domain\Product\Enums\ProductTypeEnums;
use Domain\Product\Enums\ProductUnitEnums;
use Domain\Product\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'price' => $this->faker->numberBetween(1, 500),
            'vat' => ['0', '12', '6', '25'][mt_rand(0,3)],
            'quantity' => $this->faker->numberBetween(1, 10),
            'description' => $this->faker->text(191),
            'type' => ProductTypeEnums::SERVICE->name,
            'unit' => 'st'
        ];
    }
}
