<?php

namespace App\Admin\Users\Requests;

use App\Admin\Users\Rules\OldPasswordIsCorrectRule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserPasswordRequest extends FormRequest
{
	public function rules()
	{
        $password = data_get($this->route('user'), 'password');

		return [
            'old_password' => ['required', new OldPasswordIsCorrectRule($password)],
            'new_password' => ['required'],
            'new_password_again' => ['required', 'same:new_password']
        ];
	}


	public function messages()
	{
		return [];
	}
}
