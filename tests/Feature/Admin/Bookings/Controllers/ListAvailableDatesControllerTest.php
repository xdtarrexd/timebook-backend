<?php

namespace Admin\Bookings\Controllers;

use App\Admin\Bookings\Controllers\ListAvailableDatesController;
use Domain\Booking\Models\AvailableDate;
use Tests\TestCase;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListAvailableDatesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create([
            'is_admin' => true,
        ]);
        $this->be($user);
    }

    public function testListAvailableDatesControllerTest()
    {
        AvailableDate::factory()->create();

        $res = $this->json('GET', 'admin/bookings/available-dates');

        $res->assertOk();
        $this->assertSame(1, $res->json('total'));
    }

}
