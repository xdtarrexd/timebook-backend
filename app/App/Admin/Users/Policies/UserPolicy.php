<?php

namespace App\Admin\Users\Policies;

use Domain\User\Models\User;

class UserPolicy
{
    public function viewAny(User $authedUser): bool
    {
        return true;
    }

    public function view(User $authedUser, User $user): bool
    {
        return $authedUser->is_admin;
    }

    public function create(User $authedUser): bool
    {
        return $authedUser->is_admin;
    }

    public function update(User $authedUser, User $user): bool
    {
        return $authedUser->is_admin;
    }

    public function delete(User $authedUser, User $user): bool
    {
        return $authedUser->is_admin;
    }

    public function updatePassword(User $authedUser, User $user): bool
    {
        return $authedUser->is_admin || ($authedUser->id == $user->id);
    }
}
