<?php

namespace Domain\Booking\Models;

use Domain\Booking\Observers\AvailableDateTimeObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Support\Models\BaseModelTrait;

/**
 * Domain\Booking\Models\AvailableDateTime
 *
 * @property int $id
 * @property int $available_date_id
 * @property string $time
 * @property string $duration
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Domain\Booking\Models\AvailableDate $AvailableDate
 * @method static \Database\Factories\AvailableDateTimeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime query()
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime whereAvailableDateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime whereDuration($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime whereTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDateTime whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AvailableDateTime extends Model
{
    use BaseModelTrait, HasFactory;

    public $guarded = [];


    protected static function boot()
    {
        parent::boot();
        self::observe(AvailableDateTimeObserver::class);
    }


    public function AvailableDate(): BelongsTo
    {
        return $this->belongsTo(AvailableDate::class);
    }
}
