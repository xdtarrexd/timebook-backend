<?php

namespace App\Admin\Bookings\Queries;

use Domain\Booking\Models\AvailableDate;
use Support\BaseQueryBuilder;

class ListAvailableDatesQuery extends BaseQueryBuilder
{
	public function base()
	{
		return AvailableDate::class;
	}
}
