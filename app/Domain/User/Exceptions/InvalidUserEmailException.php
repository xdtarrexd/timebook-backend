<?php

namespace Domain\User\Exceptions;

use Support\Exceptions\DomainException;

class InvalidUserEmailException extends DomainException
{

}