<?php

namespace App\Admin\Users\Controllers;

use App\Admin\Users\Policies\UserPolicy;
use App\Admin\Users\Requests\UpdateUserPasswordRequest;
use Domain\User\Actions\UpdateUserPasswordAction;
use Domain\User\Models\User;
use Support\Http\AuthorizeSingleControllerMethod;

class UpdateUserPasswordController extends AuthorizeSingleControllerMethod
{
    protected $model = User::class;
    protected $policy = UserPolicy::class;

    /**
     * @throws \Domain\User\Exceptions\InvalidOldPasswordProvidedException
     */
    public function __invoke(UpdateUserPasswordRequest $request, UpdateUserPasswordAction $action, User $user)
    {
        $this->authorize('updatePassword', $user);

        $action($user,
            $request->get('old_password'),
            $request->get('new_password')
        );
    }
}
