<?php

namespace App\Admin\Users\Rules;

use Hash;
use Illuminate\Contracts\Validation\Rule;

class OldPasswordIsCorrectRule implements Rule
{
    private string $hashedPassword;

    public function __construct(string $hashedPassword)
    {

        $this->hashedPassword = $hashedPassword;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return Hash::check($value, $this->hashedPassword);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Wrong password';
    }
}