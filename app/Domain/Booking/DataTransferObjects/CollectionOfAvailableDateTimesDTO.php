<?php

namespace Domain\Booking\DataTransferObjects;

use Exception;
use Spatie\DataTransferObject\Caster;

class CollectionOfAvailableDateTimesDTO implements Caster
{

    public function cast(mixed $value): array
    {
        if (! is_array($value)) {
            throw new Exception("Can only cast arrays to Foo");
        }

        return array_map(
            fn (array $data) => new AvailableDateTimeDTO(...$data),
            $value
        );
    }
}