<?php

namespace Domain\Customer\Exceptions;

use Support\Exceptions\DomainException;

class InvalidCustomerMailException extends DomainException
{
}
