<?php

namespace Product\Actions;

use Domain\Product\Actions\UpdateProductAction;
use Domain\Product\DataTransferObjects\ProductDTO;
use Domain\Product\Enums\ProductTypeEnums;
use Domain\Product\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateProductActionTest extends TestCase
{
    use RefreshDatabase;

    public function testUpdateProductActionTest()
    {
        $product = Product::factory()->create();
        $oldVal = $product->name;
        $dto = new ProductDTO(
            name: 'Klipp herr 60 min',
            price: '320',
            vat: '25',
            quantity: 60,
            unit: 'st',
            type: ProductTypeEnums::SERVICE->toString(),
            description: '60 min med rakapparat',
        );

        $this->assertNotSame($oldVal, $dto->name);
        (new  UpdateProductAction)($product, $dto);
        $product->refresh();
        $this->assertSame($dto->name, $product->name);


    }

}
