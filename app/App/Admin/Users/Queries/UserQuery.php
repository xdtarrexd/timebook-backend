<?php

namespace App\Admin\Users\Queries;

use Domain\User\Models\User;
use Support\BaseQueryBuilder;

class UserQuery extends BaseQueryBuilder
{
	public function base()
	{
		return User::class;
	}
}
