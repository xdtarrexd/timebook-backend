<?php

namespace Domain\Customer\Actions;

use Domain\Customer\DataTransferObjects\CustomerDTO;
use Domain\Customer\Models\Customer;

class UpdateCustomerAction
{
	public function __invoke(Customer $customer, CustomerDTO $DTO): void
	{
		$customer->fromDTO($DTO);
        $customer->save();
	}
}
