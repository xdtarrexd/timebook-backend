<?php

namespace Domain\User\Observers;

use Domain\User\Exceptions\InvalidUserEmailException;
use Domain\User\Models\User;
use Str;

class UserObserver
{
    public function creating(User $user)
    {
        if (!$user->access_token) {
            $user->access_token = (string) Str::orderedUuid();
        }

        if (is_null($user->is_admin)) {
            $user->is_admin = false;
        }
        if (is_null($user->is_bookable)) {
            $user->is_bookable = true;
        }

        if (!filter_var($user->email, FILTER_VALIDATE_EMAIL)) throw new InvalidUserEmailException;
    }
}