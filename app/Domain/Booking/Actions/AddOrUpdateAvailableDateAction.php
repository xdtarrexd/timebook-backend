<?php

namespace Domain\Booking\Actions;

use Domain\Booking\DataTransferObjects\AvailableDateDTO;
use Domain\Booking\DataTransferObjects\AvailableDateTimeDTO;
use Domain\Booking\Models\AvailableDate;
use Domain\Booking\Models\AvailableDateTime;

class AddOrUpdateAvailableDateAction
{
    public function __invoke(AvailableDateDTO $DTO): AvailableDate
    {
        /*
         * Update date entry
         */
        $availableDate = AvailableDate::firstOrNew(['date' => $DTO->date]);
        $availableDate->fromDTO($DTO->except('times'));
        $availableDate->save();

        /*
         * (Remove previous times)
         */
        AvailableDateTime::whereAvailableDateId($availableDate->id)->delete();
        /*
         * Fill new ones
         */
        foreach ($DTO->times as $time) {
            $availableDateTime = AvailableDateTime::firstOrNew([
                'time' => $time->time,
                'available_date_id' => $availableDate->id
            ]);
            $availableDateTime->fromDTO($time);
            $availableDateTime->save();
        }

        return $availableDate;
    }
}
