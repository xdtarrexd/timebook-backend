<?php

namespace Domain\User\Actions;

use Domain\User\DataTransferObjects\UserDTO;
use Domain\User\Models\User;

class UpdateUserAction
{
    public function __invoke(User $user, UserDTO $DTO): void
    {
        $user->fromDTO($DTO);

        $user->save();
    }

}