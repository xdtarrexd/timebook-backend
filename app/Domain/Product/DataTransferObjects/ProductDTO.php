<?php

namespace Domain\Product\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

final class ProductDTO extends DataTransferObject
{
    public string $name;
    public string $price;
    public string $vat;
    public string $quantity;
    public string $unit;
    public string $type;
    public ?string $description = null;

    /**
     * @param Request $request
     * @return static
     */
    public static function fromRequest(Request $request): self
    {
        return new self(
            name: $request->get('name'),
            price: $request->get('price'),
            vat: $request->get('vat'),
            quantity: $request->get('quantity'),
            unit: $request->get('unit'),
            type: $request->get('type'),
            description: $request->get('description'),
        );
    }

}
