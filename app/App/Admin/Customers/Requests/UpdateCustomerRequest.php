<?php

namespace App\Admin\Customers\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCustomerRequest extends FormRequest
{
	public function rules()
	{
		return [
            'name' => ['required'],
            'email' => ['required', 'email'],
            'phone' => ['required', 'starts_with:+46']
        ];
	}


	public function messages()
	{
		return [];
	}
}
