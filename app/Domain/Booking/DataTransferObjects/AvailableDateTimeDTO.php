<?php

namespace Domain\Booking\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

final class AvailableDateTimeDTO extends DataTransferObject
{
	public string $time;
	public string $duration;

}
