<?php

namespace App\Admin\Login\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
	public function rules()
	{
		return [
            'username' => ['required'],
            'password' => ['required']
        ];
	}


	public function messages()
	{
		return [];
	}
}
