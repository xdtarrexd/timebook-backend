<?php

namespace Booking\Actions;

use Domain\Booking\Actions\AddOrUpdateAvailableDateAction;
use Domain\Booking\DataTransferObjects\AvailableDateDTO;
use Domain\Booking\Models\AvailableDate;
use Domain\Booking\Models\AvailableDateTime;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AddOrUpdateAvailableDateActionTest extends TestCase
{
    use RefreshDatabase;


    public function testAddOrUpdateAvailableDateActionTest()
    {

        $AvailableDate = new AvailableDateDTO(
            date: '2022-02-20',
            published: false,
            times: [
                [
                    'time' => '15:45',
                    'duration' => '60'
                ],
                [
                    'time' => '16:30',
                    'duration' => '90'
                ]
            ]
        );

        $res = (new AddOrUpdateAvailableDateAction)($AvailableDate);

        $this->assertInstanceOf(AvailableDate::class, $res);
        $this->assertSame(2, $res->AvailableDateTimes->count());
        $this->assertInstanceOf(AvailableDateTime::class, $res->AvailableDateTimes[0]);


    }

}
