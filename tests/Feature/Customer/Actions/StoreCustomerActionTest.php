<?php

namespace Customer\Actions;

use Domain\Customer\Actions\StoreCustomerAction;
use Domain\Customer\DataTransferObjects\CustomerDTO;
use Domain\Customer\Models\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StoreCustomerActionTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreCustomerActionTest()
    {
        $dto = new CustomerDTO(
            name: 'Tarre',
            phone: '+46701123',
            email: 'tarre.islam@gmail.com',
        );

        $res = (new StoreCustomerAction)($dto);

        $this->assertInstanceOf(Customer::class, $res);
    }

}
