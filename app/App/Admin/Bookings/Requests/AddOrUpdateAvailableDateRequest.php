<?php

namespace App\Admin\Bookings\Requests;

use App\Admin\Bookings\Rules\MilitaryTimeRule;
use Illuminate\Foundation\Http\FormRequest;

class AddOrUpdateAvailableDateRequest extends FormRequest
{
	public function rules()
	{
		return [
            'date' => ['required', 'date'],
            'published' => ['required', 'boolean'],
            'times' => ['required', 'array'],
            'times.*.time' => ['required', new MilitaryTimeRule],
            'times.*.duration' => ['required']
        ];
	}


	public function messages()
	{
		return [];
	}
}
