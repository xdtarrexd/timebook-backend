<?php

namespace Product\Actions;

use Domain\Product\Actions\DeleteProductAction;
use Domain\Product\Models\Product;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteProductActionTest extends TestCase
{
    use RefreshDatabase;


    public function testDeleteProductActionTest()
    {
        $product = Product::factory()->create();

        (new DeleteProductAction)($product);

        $this->assertDeleted($product);

    }

}
