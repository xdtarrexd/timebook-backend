<?php

namespace User\Actions;

use Domain\User\Actions\LoginUserActonAction;
use Domain\User\DataTransferObjects\LoginDTO;
use Domain\User\Exceptions\InvalidUsernameOrPasswordException;
use Exception;
use Tests\TestCase;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginUserActonActionTest extends TestCase
{
    use RefreshDatabase;


    public function testLoginUserActonActionWithWrongCredentials()
    {
        User::factory()->create([
            'email' => $username = 'godspeed@ads.com',
            'password' => $password = 'Hej123Tjennaa'
        ]);

        $req = new LoginDTO(
            username: $username,
            password: $password . 'Typo'
        );

        try {
            (new LoginUserActonAction)($req);
        } catch (Exception $exception) {
            $this->assertInstanceOf(InvalidUsernameOrPasswordException::class, $exception);
        }


    }


    public function testLoginUserActonActionWithWCorrectCredentials()
    {
        User::factory()->create([
            'email' => $username = 'godspeed@ads.com',
            'password' => $password = 'Hej123Tjennaa'
        ]);

        $req = new LoginDTO(
            username: $username,
            password: $password
        );


        $res = (new LoginUserActonAction)($req);

        $this->assertInstanceOf(User::class, $res);

    }

}
