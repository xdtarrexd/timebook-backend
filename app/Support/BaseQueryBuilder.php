<?php

namespace Support;

use Closure;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection as SupportCollection;
use Laravel\Scout\Builder as ScoutBuilder;


abstract class BaseQueryBuilder
{
    protected Request $request;

    public int $resultsPerPage = 15;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Class FQDN or base for query.
     * @return mixed
     */
    public abstract function base();

    /**
     * @param $request
     * @return $this
     */
    public function withCustomRequest(Request $request)
    {
        $this->request = $request;
        return $this;
    }

    public function withScoutBuilder(ScoutBuilder $builder): ScoutBuilder
    {
        return $builder;
    }

    public function mapper(): ?Closure
    {
        return null;
    }

    public function wrapper(): ?Closure
    {
        return null;
    }

    /**
     * @param false $forPagination
     * @return Model|QueryBuilder|SupportCollection
     * @throws \Exception
     */
    public final function get($forPagination = false)
    {
        $model = $this->model();
        /*
         * Figure out how to get all results
         */
        if ($model instanceof Model || $model instanceof QueryBuilder || $model instanceof Relation || $model instanceof EloquentBuilder) {
            if ($forPagination) {
                return $model;
            }
            $items = $model->get();
        } elseif ($model instanceof SupportCollection) {
            $items = $model;
        } else {
            throw new \Exception('No mames');
        }
        /*
         * Map result
         */
        if (!is_null($mapper = $this->mapper())) {
            $items = $items->map($mapper);
        }
        /*
         * Wrap result
         */
        if (!is_null($wrapper = $this->wrapper())) {
            $items = $wrapper($items);
        }
        /*
         * Return collection
         */
        return $items;
    }

    /**
     * @return ScoutBuilder|null
     */
    public final function search(): ?ScoutBuilder
    {
        $mdl = $this->base();
        $search = $this->request->get('search');
        return $this->withScoutBuilder($mdl::search($search, $this->mapper()));
    }

    /**
     * @return LengthAwarePaginator
     * @throws \Exception
     */
    public final function paginate(): LengthAwarePaginator
    {
        $perPage = $this->request->get('perPage', $this->resultsPerPage);
        /*
         * Grab base model
         */
        $items = $this->get(true);
        /*
         * Models and query builders asking for pagination, we serve them via the models,
         */
        if ($items instanceof QueryBuilder || $items instanceof Model || $items instanceof Relation || $items instanceof EloquentBuilder) {
            /*
            * Map result
            */
            if (!is_null($mapper = $this->mapper())) {
                echo "Mapper does not work here lol";
                // dd('Mapper does not work here lol');
            }
            return $items->paginate();
        }
        /*
         * Get pages
         */
        $page = $this->request->get('page');
        /*
         * Paginate
         */
        $items = $items->forPage($page, $perPage);
        return new LengthAwarePaginator($items, $items->count(), $perPage, $page, [
            'path' => $this->request->url(),
        ]);

    }

    protected function model()
    {
        $model = $this->base();
        if (is_string($model) && class_exists($model)) {
            $model = new $model;
        }
        return $model;
    }

}
