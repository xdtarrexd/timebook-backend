<?php

namespace App\Admin\Bookings\Rules;

use Illuminate\Contracts\Validation\Rule;
use Support\ValidatorHelpers\CheckIfMilitaryTime;

class MilitaryTimeRule implements Rule
{
	public function passes($attribute, $value)
	{
		return CheckIfMilitaryTime::validate($value);
	}


	public function message()
	{
		return [
            'Invalid time'
        ];
	}
}
