<?php

namespace App\Admin\Users\Controllers;

use App\Admin\Users\Policies\UserPolicy;
use App\Admin\Users\Queries\UserQuery;
use App\Admin\Users\Requests\StoreUserRequest;
use Domain\User\Actions\DeleteUserAction;
use Domain\User\Actions\StoreUserAction;
use Domain\User\Actions\UpdateUserAction;
use Domain\User\DataTransferObjects\UserDTO;
use Domain\User\Models\User;
use Support\Http\AuthorizeCurdController;


class UserController extends AuthorizeCurdController
{
    protected $model = User::class;
    protected $policy = UserPolicy::class;

    public function index(UserQuery $query)
    {
        return $query->search()->paginate();
    }

    public function show(User $user)
    {
        return $user;
    }

    public function store(StoreUserRequest $request, StoreUserAction $action)
    {
        $dto = UserDTO::fromRequest($request);
        return $action($dto);
    }

    public function update(StoreUserRequest $request, User $user, UpdateUserAction $action)
    {
        $dto = UserDTO::fromRequest($request);
        $action($user, $dto);
    }

    public function destroy(User $user, DeleteUserAction $action)
    {
        $action($user);
    }
}