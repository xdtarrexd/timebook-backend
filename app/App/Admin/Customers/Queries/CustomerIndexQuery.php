<?php

namespace App\Admin\Customers\Queries;

use Domain\Customer\Models\Customer;
use Support\BaseQueryBuilder;

class CustomerIndexQuery extends BaseQueryBuilder
{
    public function base()
    {
        return Customer::class;
    }
}
