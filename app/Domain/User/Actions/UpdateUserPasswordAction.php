<?php

namespace Domain\User\Actions;

use Domain\User\Exceptions\InvalidOldPasswordProvidedException;
use Domain\User\Models\User;
use Hash;

class UpdateUserPasswordAction
{
    /**
     * @throws InvalidOldPasswordProvidedException
     */
    public function __invoke(User $user, $oldPassword, $newPassword): void
    {
        if (!Hash::check($oldPassword, $user->password)) {
            throw new InvalidOldPasswordProvidedException;
        }
        $user->password = $newPassword;
        $user->save();
    }

}