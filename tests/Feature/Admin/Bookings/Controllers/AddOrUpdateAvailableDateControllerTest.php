<?php

namespace Admin\Bookings\Controllers;

use App\Admin\Bookings\Controllers\AddOrUpdateAvailableDateController;
use Tests\TestCase;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AddOrUpdateAvailableDateControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create([
            'is_admin' => true,
        ]);
        $this->be($user);
    }

    public function testAddOrUpdateAvailableDateControllerTest()
    {
        $res = $this->json('POST', 'admin/bookings/available-dates/add-or-update', [
            'date' => '2022-02-20',
            'published' => false,
            'times' => [
                [
                    'time' => '15:45',
                    'duration' => '60'
                ],
                [
                    'time' => '16:30',
                    'duration' => '90'
                ]
            ]
        ]);

        $res->assertCreated();

    }

}
