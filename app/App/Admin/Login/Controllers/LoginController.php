<?php

namespace App\Admin\Login\Controllers;

use App\Admin\Login\Requests\LoginRequest;
use App\Admin\Login\Resources\AuthedUserResource;
use Domain\User\Actions\LoginUserActonAction;
use Domain\User\DataTransferObjects\LoginDTO;

class LoginController
{
    /**
     * @param LoginRequest $request
     * @param LoginUserActonAction $action
     * @return AuthedUserResource
     * @throws \Domain\User\Exceptions\InvalidUsernameOrPasswordException
     */
    public function __invoke(LoginRequest $request, LoginUserActonAction $action)
	{
        $DTO = LoginDTO::fromRequest($request);
		$user = $action($DTO);

        return AuthedUserResource::make(['user' => $user]);
	}
}
