<?php

namespace Domain\User\Exceptions;

use Support\Exceptions\DomainException;

class InvalidOldPasswordProvidedException extends DomainException
{
}
