<?php

namespace Tests\Unit\User\Actions;

use Domain\User\Actions\StoreUserAction;
use Domain\User\DataTransferObjects\UserDTO;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class StoreUserActionTest extends TestCase
{
    use RefreshDatabase;

    public function testStoreUser()
    {
        $user = new UserDTO([
            'name' => 'Tarre',
            'email' => 'heheh',
            'is_bookable' => false,
            'is_admin' => false,
        ]);

        $user = (new StoreUserAction)($user);

        $this->assertInstanceOf(User::class, $user);

    }

}
