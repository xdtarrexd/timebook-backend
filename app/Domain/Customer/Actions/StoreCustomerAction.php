<?php

namespace Domain\Customer\Actions;

use Domain\Customer\DataTransferObjects\CustomerDTO;
use Domain\Customer\Models\Customer;

class StoreCustomerAction
{
	public function __invoke(CustomerDTO $DTO): Customer
	{
		$customer = (new Customer)->fromDTO($DTO);
        $customer->save();
        return $customer;
	}
}
