<?php

namespace Admin\Users\Controllers;

use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create([
            'is_admin' => true,
        ]);
        $this->be($user);
    }


    public function testIndex()
    {
        $res = $this->json('GET', 'admin/users');
        $res->assertOk();
    }

    public function testStore()
    {
        $res = $this->json('POST', 'admin/users', [
           'email' => 'asdm@gmail.com',
           'name' => 'Tariqul Islam',
           'is_bookable' => true,
           'is_admin' => false,
        ]);

        $res->assertOk();
    }

    public function testUpdate()
    {
        $user = User::factory()->create();

        $res = $this->json('PUT', "admin/users/{$user->id}", [
            'email' => 'new_email@gmail.com',
            'name' => 'New name',
            'is_bookable' => true,
            'is_admin' => true,
        ]);

        $res->assertOk();
    }

    public function testShow()
    {
        $user = User::factory()->create();

        $res = $this->json('GET', "admin/users/{$user->id}");

        $res->assertOk();

    }


    public function testDestroy()
    {
        $user = User::factory()->create();

        $res = $this->json('DELETE', "admin/users/{$user->id}");

        $res->assertOk();
    }

}
