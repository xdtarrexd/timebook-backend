<?php

namespace Domain\Booking\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Laravel\Scout\Searchable;
use Support\Models\BaseModelTrait;

/**
 * Domain\Booking\Models\AvailableDate
 *
 * @property int $id
 * @property string $date
 * @property bool $published
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Domain\Booking\Models\AvailableDateTime[] $AvailableDateTimes
 * @property-read int|null $available_date_times_count
 * @method static \Database\Factories\AvailableDateFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate query()
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AvailableDate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AvailableDate extends Model
{
    use Searchable, BaseModelTrait, HasFactory;

	public $guarded = [];

    public $casts = [
        'published' => 'boolean'
    ];

	protected static function boot()
	{
		parent::boot();
	}

    public function AvailableDateTimes(): HasMany
    {
        return $this->hasMany(AvailableDateTime::class);
    }
}
