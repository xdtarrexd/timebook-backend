<?php


use App\Admin\Bookings\Controllers\AddOrUpdateAvailableDateController;
use App\Admin\Bookings\Controllers\ListAvailableDatesController;
use App\Admin\Bookings\Controllers\ListAvailableDateTimesController;
use App\Admin\Customers\Controllers\CustomerController;
use App\Admin\Login\Controllers\LoginController;
use App\Admin\Users\Controllers\UpdateUserPasswordController;
use App\Admin\Users\Controllers\UserController;
use Support\Http\Middleware\AuthenticateAdminRouteMiddleware;

/*
 * Login
 */
Route::post('login', LoginController::class);
/*
 * Authed place
 */
Route::middleware(AuthenticateAdminRouteMiddleware::class)->group(function () {
    /*
     * CUstomers
     */
    Route::apiResource('customers', CustomerController::class)->except('store');
    /*
     * Users
     */
    Route::apiResource('users', UserController::class);
    Route::prefix('users/{user}')->group(function () {
        Route::patch('update-password', UpdateUserPasswordController::class);
    });
    /*
     * Booking
     */
    Route::prefix('bookings')->group(function () {
        Route::prefix('available-dates')->group(function () {
            Route::get('/', ListAvailableDatesController::class);
            Route::post('/add-or-update', AddOrUpdateAvailableDateController::class);
            Route::prefix('{available_date}')->group(function(){
                Route::get('times', ListAvailableDateTimesController::class);
            });
        });
    });

});
