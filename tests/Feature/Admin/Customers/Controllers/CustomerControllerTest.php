<?php

namespace Admin\Customers\Controllers;

use App\Admin\Customers\Controllers\CustomerController;
use Domain\Customer\Models\Customer;
use Tests\TestCase;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CustomerControllerTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $user = User::factory()->create([
            'is_admin' => true,
        ]);
        $this->be($user);

    }

    public function testIndex()
    {
        $res = $this->json('GET', 'admin/customers');
        $res->assertOk();
    }


    public function testShow()
    {
        $customer = Customer::factory()->create();

        $res = $this->json('GET', "admin/customers/$customer->id");
        $res->assertOk();

    }

    public function testUpdate()
    {
        $customer = Customer::factory()->create();

        $res = $this->json('PUT', "admin/customers/$customer->id", [
            'name' => 'New name',
            'phone' => '+461234',
            'email' => 'tarre.barre@gmail.com'
        ]);
        $res->assertOk();
    }

    public function testDestroy()
    {
        $customer = Customer::factory()->create();

        $res = $this->json('DELETE', "admin/customers/$customer->id");
        $res->assertOk();
    }

}
