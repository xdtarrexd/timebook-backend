<?php

namespace Domain\Customer\Observers;

use Domain\Customer\Exceptions\InvalidCustomerMailException;
use Domain\Customer\Exceptions\InvalidCustomerPhoneException;
use Domain\Customer\Models\Customer;
use Str;

class CustomerObserver
{
    public function creating(Customer $customer)
    {
        if (!filter_var($customer->email, FILTER_VALIDATE_EMAIL)) {
            throw new InvalidCustomerMailException;
        }

        if (!Str::startsWith($customer->phone, '+')) {
            throw new InvalidCustomerPhoneException;
        }
    }


}
