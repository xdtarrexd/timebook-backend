<?php

namespace Support\Http\Middleware;
use Illuminate\Http\Request;
use Support\Exceptions\DomainException;


class HandleDomainExceptions
{
    public function handle(Request $request, $next)
    {
        try {
            return $next($request);
        } catch (DomainException $domainException){
            abort(500, $domainException->getMessage());
        }
    }
}