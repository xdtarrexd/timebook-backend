<?php

namespace Customer\Actions;

use Domain\Customer\Actions\UpdateCustomerAction;
use Domain\Customer\DataTransferObjects\CustomerDTO;
use Domain\Customer\Models\Customer;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UpdateCustomerActionTest extends TestCase
{
    use RefreshDatabase;


    public function testUpdateCustomerActionTest()
    {
        $dto = new CustomerDTO(
            name: 'Tarre',
            phone: '+46701123',
            email: 'tarre.islam@gmail.com',
        );
        $customer = Customer::factory()->create([
            'name' => 'Not tarre'
        ]);

        (new UpdateCustomerAction)($customer, $dto);

        $this->assertSame($dto->name, $customer->name);
    }

}
