<?php

namespace Tests\Unit\User\Actions;

use Domain\User\Actions\DeleteUserAction;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DeleteUserActionTest extends TestCase
{
    use RefreshDatabase;

    public function testDeleteUserActionTest()
    {
        $user = User::factory()->create();

        (new DeleteUserAction)($user);

        $this->assertDeleted($user);

    }

}
