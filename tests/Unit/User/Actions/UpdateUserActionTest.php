<?php

namespace Tests\Unit\User\Actions;

use Domain\User\Actions\UpdateUserAction;
use Domain\User\DataTransferObjects\UserDTO;
use Domain\User\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateUserActionTest extends TestCase
{
    use RefreshDatabase;

    public function testUpdateUserAction()
    {
        $user = User::factory()->create();

        $updateUserDTO = new UserDTO([
           'name' => 'Tarre',
           'email' => 'tarre.islam@gmail.com',
            'is_bookable' => false,
            'is_admin' => false,
        ]);

        $this->assertNotSame($user->name, $updateUserDTO->name);
        $this->assertNotSame($user->email, $updateUserDTO->email);

        (new UpdateUserAction)($user, $updateUserDTO);
        $user->refresh();

        $this->assertSame($user->name, $updateUserDTO->name);
        $this->assertSame($user->email, $updateUserDTO->email);

    }

}
