<?php

namespace Domain\User\States\User;

class Inactive extends UserState
{

    public function color(): string
    {
        return 'red';
    }
}