<?php

namespace Domain\Customer\DataTransferObjects;

use Illuminate\Http\Request;
use Spatie\DataTransferObject\DataTransferObject;

final class CustomerDTO extends DataTransferObject
{
	public string $name;
	public ?string $phone;
	public ?string $email;


	/**
	 * @param Request $request
	 * @return static
	 */
	public static function fromRequest(Request $request): self
	{
		return new self([
		    'name' => $request->get('name'),
		    'phone' => $request->get('phone'),
		    'email' => $request->get('email'),
		]);
	}

}
