<?php

namespace Domain\Booking\Exceptions;

use Support\Exceptions\DomainException;

class InvalidDateTimeException extends DomainException
{
}
