<?php

namespace Domain\Customer\Exceptions;

use Support\Exceptions\DomainException;

class InvalidCustomerPhoneException extends DomainException
{
}
