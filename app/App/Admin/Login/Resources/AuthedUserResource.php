<?php

namespace App\Admin\Login\Resources;

use Domain\User\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthedUserResource extends JsonResource
{
    public function toArray($request)
    {
        $user = $this->getUser();

        self::$wrap = null;
        return [
            'access_token' => $user->access_token,
            'name' => $user->name,
        ];
    }

    public function getUser(): User
    {
        return $this['user'];
    }
}
