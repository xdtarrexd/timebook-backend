<?php

namespace App\Admin\Bookings\Controllers;

use App\Admin\Bookings\Policies\AvailableDatePolicy;
use App\Admin\Bookings\Requests\AddOrUpdateAvailableDateRequest;
use Domain\Booking\Actions\AddOrUpdateAvailableDateAction;
use Domain\Booking\DataTransferObjects\AvailableDateDTO;
use Domain\Booking\Models\AvailableDate;
use Support\Http\AuthorizeSingleControllerMethod;

class AddOrUpdateAvailableDateController extends AuthorizeSingleControllerMethod
{
    protected $model = AvailableDate::class;
    protected $policy = AvailableDatePolicy::class;

    /**
     * @param AddOrUpdateAvailableDateRequest $request
     * @param AddOrUpdateAvailableDateAction $action
     * @return \Domain\Booking\Models\AvailableDate
     */
    public function __invoke(AddOrUpdateAvailableDateRequest $request, AddOrUpdateAvailableDateAction $action)
    {
        $this->authorize('addOrUpdate', $this->model);
        $DTO = AvailableDateDTO::fromRequest($request);
        return $action($DTO);
    }
}
